//
//  ViewController.swift
//  WebviewSample
//
//  Created by Deep Arora on 13/02/19.
//  Copyright © 2019 Deep Arora. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    
    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
      // let userAgentApple = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1 redmart-ios"
        
        let userAgentApple = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.1 Safari/605.1.15"
     //   UserDefaults.standard.register(defaults: ["UserAgent": userAgentApple])
        
      //  let urlString = "http://enable-mobile-chat.alpha.redmart.com/customer-support/"
        let urlString = "https://redmart.com/customer-support/"
        let request = URLRequest(url: URL(string: urlString)!)
       // webview.load(request)
        webview.loadRequest(request)
    }


 
}

